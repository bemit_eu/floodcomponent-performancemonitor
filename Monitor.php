<?php

namespace Flood\Component\PerformanceMonitor;

/**
 * Saves Performance Profiles and Results
 *
 * @category
 * @package    \Flood\Component\PerformanceMonitor
 * @author     michael@bemit.codes
 * @link
 * @copyright  2017 Michael Becker
 * @version    0.5.0
 */
class Monitor {

    /**
     * Saves the performance profiles and their informations
     *
     * @var array
     */
    protected $profile = [];

    protected $resource = false;

    /**
     * When only used once in project you could make usage of singleton, but serving from own container is recommended
     *
     * @var null|self
     */
    protected static $i = null;

    /**
     * Protected as only one exists normally per runtime
     */
    public function __construct() {
    }

    /**
     * Singleton funciont, returns one instace of this class or initializes that one instance
     *
     * @return self
     */
    public static function i() {
        if(null === static::$i) {
            static::$i = new self;
        }

        return static::$i;
    }

    public function setResource($id = false) {
        if($id) {
            $this->resource = $id;
        } else {
            $this->resource = $this->findResource();
        }
    }

    public function findResource() {
        if('cli' === php_sapi_name()) {
            $resource = implode(' ', $_SERVER['argv']);
        } else {
            $ssl = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on');
            $port = $_SERVER['SERVER_PORT'];
            $port = ((!$ssl && $port == '80') || ($ssl && $port == '443')) ? '' : ':' . $port;
            $resource =
                $_SERVER['REQUEST_METHOD'] . ':' .
                'http' . ($ssl ? 's' : '') . '://' . (
                isset($_SERVER['HTTP_X_FORWARDED_HOST']) ?
                    $_SERVER['HTTP_X_FORWARDED_HOST'] :
                    (isset($_SERVER['HTTP_HOST']) ?
                        $_SERVER['HTTP_HOST'] :
                        $_SERVER['SERVER_NAME'] . $port)
                ) . $_SERVER['REQUEST_URI'];
        }

        return $resource;
    }

    public function getResource() {
        return $this->resource;
    }

    public function runProfiled($name, $exec) {
        $this->startProfile($name);

        call_user_func($exec);

        $this->endProfile($name);
    }

    /**
     * Starts a main profile
     *
     * @param string     $name
     * @param null|array $data
     */
    public function startProfile($name, $data = null) {
        if(isset($data['memory'])) {
            $this->profile[$name]['start']['memory'] = $data['memory'];
        } else {
            $this->profile[$name]['start']['memory'] = memory_get_usage();
        }

        if(isset($data['time'])) {
            $this->profile[$name]['start']['time'] = $data['time'];
        } else {
            $this->profile[$name]['start']['time'] = microtime(true);
        }
    }

    /**
     * Ends a profile, when no data is set it collects the runtime information inside of this method
     *
     * @param string     $name
     * @param null|array $data
     */
    public function endProfile($name, $data = null) {
        if(isset($data['memory'])) {
            $this->profile[$name]['end']['memory'] = $data['memory'];
        } else {
            $this->profile[$name]['end']['memory'] = memory_get_usage();
        }

        if(isset($data['time'])) {
            $this->profile[$name]['end']['time'] = $data['time'];
        } else {
            $this->profile[$name]['end']['time'] = microtime(true);
        }
    }

    /**
     * Returns the comsumed memory of the given profile id
     *
     * @param string $name the profile id
     *
     * @return mixed
     */
    public function getMemory($name) {
        return round($this->profile[$name]['end']['memory'] - $this->profile[$name]['start']['memory'], 6);
    }

    /**
     * Returns the needed time of the given profile id
     *
     * @param string $name the profile id
     *
     * @return mixed
     */
    public function getTime($name) {
        return round($this->profile[$name]['end']['time'] - $this->profile[$name]['start']['time'], 6);
    }

    /**
     * Converts a given size from memory_get_usage to kb or mb
     *
     * @param $size
     *
     * @return string size with unit
     *
     * @author xelozz -at- gmail.com | from php.net
     */
    public function convertMemory($size) {
        $unit = ['b', 'kb', 'mb', 'gb', 'tb', 'pb'];
        return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
    }

    /**
     * Calculates all the data information about started profiles, time with negative value (-0.0001) means that the time was too fast for measurment
     *
     * @param null $name when the information only for a specific profile
     *
     * @return bool|array the calculated monitor information, memory in bytes and time in seconds, when time = 0.0001 it is the smallest ammount recordable as less would be in a format like  '7.8E-5'
     */
    public function getInformation($name = null) {
        $return_val = false;
        if(null === $name) {
            foreach($this->profile as $name => $data) {
                if(0.0001 > $this->getTime($name)) {
                    $time = 0.0001;
                } else {
                    $time = $this->getTime($name);
                }
                $return_val[$name] = [
                    'memory' => $this->getMemory($name),
                    'time' => $time,
                ];
            }
        } else {
            if(0.0001 > $this->getTime($name)) {
                $time = 0.0001;
            } else {
                $time = $this->getTime($name);
            }
            $return_val = [
                'memory' => $this->getMemory($name),
                'time' => $time,
            ];
        }
        return $return_val;
    }
}