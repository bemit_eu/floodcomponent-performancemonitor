<?php

use Flood\Component\PerformanceMonitor\Monitor;
use Flood\Captn;

if(function_exists('captnIsSteering') && captnIsSteering()) {
    Captn\EventDispatcher::on(
        'hydro.flow.registerPerformanceMonitor',
        static function($data) {
            /**
             * @var \Flood\Component\Container\iContainer $container
             */
            $container = $data['container'];
            $container::i()->register('performance-monitor', Monitor::i());
        }
    );
}