# README

Flood Component Performance Monitor

`composer require flood/component-performancemonitor`  

## Licence
This project is free software distributed under the terms of two licences, the CeCILL-C and the GNU Lesser General Public License. You can use, modify and/ or redistribute the software under the terms of CeCILL-C (v1) for Europe or GNU LGPL (v3) for the rest of the world.

This file and the LICENCE.* files need to be distributed and not changed when distributing.
For more informations on the Licences which are applied read: [LICENCE.md](LICENCE.md)

## Usage

#### Pre fetched runtime data

Usage with pre fetched runtime data, so init scripts and logic will also be recorded

```php
<?php
$time = microtime(true);
$memory = memory_get_usage();

use \Flood\Component\PerformanceMonitor\Monitor;
// init codes, autoloading etc.

Monitor::i()->startProfile('profile-id', ['time' => $time, 'memory' => $memory]);

// main logic which should be recorded

Monitor::i()->endProfile('profile-id');

// maybe some other things

$result = Monitor::i()->getInformation('profile-id');
```

See [result](#markdown-header-result).

#### Automatic runtime data

Usage with automatic runtime data, the data will be assigned within startProfile()

```php
<?php
use \Flood\Component\PerformanceMonitor\Monitor;
// init codes, autoloading etc.

Monitor::i()->startProfile('profile-id');

// main logic which should be recorded

Monitor::i()->endProfile('profile-id');

// maybe some other things

$result = Monitor::i()->getInformation('profile-id');
```

See [result](#markdown-header-result).

#### Result

The method `Monitor->getInformation('profile-id')` will return an array with the indices `memory` (in byte) and `time` (in second). 

When called without a profile-id it will return an array with all informations and profile-id as first index.

With `Monitor->convertMemory($memory)` the used memory in bytes can be converted to kb, mb and so on.

## Information, Support and Documentation
More informations, contacts and support for Flood could be found under [flood.bemit.codes](https://flood.bemit.codes) the documentation is located at [help.flood.bemit.codes](https://help.flood.bemit.codes).

# Made by
Michael Becker, michael@bemit.codes


`v0.5.3`